cara set up JDK
a. pada saat membuat projek baru,di intellij kita dapat memilih versi JDK yang ingin digunakan
b. pilih pada kolom JDK 
c. pilih versi JDK
d. anda juga dapat memilih versi lain dengan klik "download JDK"
e. create project nya

setup appium:
a. Jalankan terminal/command prompt.
b. Ketik npm install -g appium
c. Tekan Enter.
d. Jika sudah selesai, untuk mengecek apakah Appium sudah berhasil diinstall atau belum, ketik appium -v. Jika muncul versi Appium, maka instalasi berhasil.
referensi video: https://www.youtube.com/watch?v=x-hBpgM5je8 (windows)
referensi video: https://www.youtube.com/watch?v=7APcLr-cBM8 (mac)

download android studio dari: https://developer.android.com/studio?gclid=Cj0KCQjw7aqkBhDPARIsAKGa0oK4aNfRwmXcmsb7Mr-_QJBT6LtwlyKb0q_75iKz0uWDb78CuXw6DRMaAhE7EALw_wcB&gclsrc=aw.ds

download appium inspector dari: https://github.com/appium/appium-inspector/releases

dependencies yang digunakan:
org.junit.jupiter:junit-jupiter:5.8.2
junit:junit:4.13.2
org.junit.vintage:junit-vintage-engine:5.8.2
org.seleniumhq.selenium:selenium-java:3.141.59
io.cucumber:cucumber-java:7.12.0
io.cucumber:cucumber-junit-platform-engine:7.12.0
io.cucumber:cucumber-junit:7.12.0
io.appium:java-client:7.5.1
io.github.cdimascio:java-dotenv:5.2.2
org.apache.logging.log4j:log4j-core:2.19.0'
