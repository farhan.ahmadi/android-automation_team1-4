Feature: Login feature

  Scenario: user success login
    Given user in login page
    Then user input correct username "rubio26"
    And user input correct password "stockbit"
    And user click Login button
    Then user success login

  Scenario: user login with invalid username
    Given user in login page
    Then user input incorrect username "rubio"
    And user input correct password "stockbit"
    And user click Login button
    Then user see Login error message

  Scenario: user login with invalid password
    Given user in login page
    Then user input correct username "rubio26"
    And input wrong password "stock123"
    And user click Login button
    Then user see Login error message

  Scenario: user login with blank username and password
    Given user in login page
    And user click Login button
    Then user see Login error message
