package step_definitions;

import io.cucumber.java8.En;
import stockbit.test.page_object.LoginPage;

public class LoginStepTest implements En{

    LoginPage loginPage = new LoginPage();

    public LoginStepTest() {
        Given("^user in login page$", () -> loginPage.clickLoginEntryPoint());

        Then("^user input correct username \"([^\"]*)\"$", (String username) -> loginPage.inputUsername(username));

        And("^user input correct password \"([^\"]*)\"$", (String password) -> loginPage.inputPassword(password));

        And("^user click Login button$", () -> loginPage.clickButtonLogin() );

        Then("^user input incorrect username \"([^\"]*)\"$", (String username) -> loginPage.inputUsername(username));

        And("^input wrong password \"([^\"]*)\"$", (String password) -> loginPage.inputPassword(password));

        Then("^user see Login error message$", () -> loginPage.validateErrorLogin() );

        Then("^user success login$", () -> loginPage.validateSuccessLogin() );

    }
}
