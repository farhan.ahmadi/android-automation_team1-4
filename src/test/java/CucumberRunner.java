import  io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        //stepNotifications = true, // ini gak ada di vid tapi ada di notion
        plugin = {
                "json:build/cucumber.json",
                "pretty",
                "html:build/result"
        },
        features = {
                "classpath:features",
        })
public class CucumberRunner {
}
