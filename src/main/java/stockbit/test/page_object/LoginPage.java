package stockbit.test.page_object;

import io.github.cdimascio.dotenv.Dotenv;
import org.openqa.selenium.By;
import stockbit.test.base.BasePageObject;

public class LoginPage extends BasePageObject {

    public void clickLoginEntryPoint() {
        isDisplayed("COMPANY_LOGO");
        tap("ENTRY_POINT_LOGIN");
    }

    public void inputUsername(String username) {
        inputText("FIELD_USERNAME", username);
    }

    public void inputPassword(String password) {
        inputText("FIELD_PASSWORD", password);
    }

    public void clickButtonLogin() {
        tap("BUTTON_LOGIN");
    }

    // Validasi kalau muncul error message
    public void validateErrorLogin() {
        isDisplayed("ERROR_LOGIN_VALIDATOR");
    }

    public void validateSuccessLogin() {
        // alternatif sementara, detect skip smart login
        isDisplayed("SKIP_SMART_LOGIN_BUTTON");


        // ingin pakai ini tapi disini ada error gara2 canaryleak :(
       /* tap(By.id("com.stockbitdev.android:id/btn_smart_login_skip"));
        tap(By.id("com.stockbitdev.android:id/touch_outside"));
        isDisplayed((By.id("com.stockbitdev.android:id/ivFragmentStreamCompanyLogo")));  */
    }
}
