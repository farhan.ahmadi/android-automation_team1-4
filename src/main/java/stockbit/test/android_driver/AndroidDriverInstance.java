package stockbit.test.android_driver;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class AndroidDriverInstance {

    public static AndroidDriver<AndroidElement> androidDriver;
    // line 19-21 setup laptop mas Farhan, pakai comment ya untuk switch setup

    public static void initialize() {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability("platformName", "Android");
        /*desiredCapabilities.setCapability("platformVersion", "12");
        desiredCapabilities.setCapability("deviceName", "emulator-5554");
        desiredCapabilities.setCapability("app", "/Users/Farhan Ahmadi/Documents/auto docs/app-development-debug.apk"); */
        desiredCapabilities.setCapability("appWaitActivity", "*");
        desiredCapabilities.setCapability("autoGrantPermissions", true);
        desiredCapabilities.setCapability("autoDismissAlerts", true);

        //  line 24- 26, setup laptop Iwan, pakai comment ya untuk switch setup
        desiredCapabilities.setCapability("platformVersion", "12");
        desiredCapabilities.setCapability("deviceName", "Nexus6_A12");
        desiredCapabilities.setCapability("app", "/Files/work/app-development-debug.apk");

        try {
            androidDriver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
            androidDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
    public static void quit() {
        androidDriver.quit();
    }
}
