package stockbit.test.base;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.github.cdimascio.dotenv.Dotenv;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import stockbit.test.android_driver.AndroidDriverInstance;
import stockbit.test.utils.Constans;
import stockbit.test.utils.Utils;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;

import static java.awt.SystemColor.text;

public class BasePageObject {
    Dotenv env = Dotenv.load();
    public AndroidDriver driver() {
        return AndroidDriverInstance.androidDriver;
    }

    public AndroidElement waitStrategy(ExpectedCondition<WebElement> conditions){
        WebDriverWait wait = new WebDriverWait(driver(), Constans.TIMEOUT);
        return (AndroidElement) wait.until(conditions);
    }

    public AndroidElement waitUntilClickable(By element){
        return waitStrategy(ExpectedConditions.elementToBeClickable(element));
    }

    public AndroidElement waitUntilVisible(By element){
        return waitStrategy(ExpectedConditions.visibilityOfElementLocated(element));
    }

    public By element(String elementLocator) {
        String elementValue = Utils.ELEMENTS.getProperty(elementLocator);
        System.out.println("Element value : " + elementValue);

        if (elementLocator == null) {
            throw new NoSuchElementException("no element " + elementLocator);
        } else {
            String[] locator = elementValue.split("_");
            String locatorType = locator[0];
            String locatorValue = elementValue.substring(elementValue.indexOf("_")+1);
            switch (locatorType) {
                case "id":
                    if (Objects.equals(env.get("ENV"), "prod")){
                        return By.id("com.stockbit.android:id/"+ locatorValue); //utk apk prod
                    } else{
                        return By.id("com.stockbitdev.android:id/"+ locatorValue); //utk apk staging
                    }

                case "xpath":
                    return By.xpath(locatorValue);
                default:
                    throw new IllegalStateException("Unexpected value: " + locatorType);
            }
        }
    }

    public void inputText (String locator, String text){
        waitUntilVisible(element(locator)).sendKeys(text);
        //line berikut ini harusnya dihapus nanti, buat catatan dulu
        //driver().findElement(element(locator)).sendKeys(text);
    }
    public void tap (String locator){
        waitUntilClickable(element(locator)).click();
        //line berikut ini harusnya dihapus nanti, buat catatan dulu
        // driver().findElement(element(locator)).click();
    }
    public void isDisplayed (String locator){
        By element = element(locator);
        try {
            waitUntilVisible(element);
        } catch (TimeoutException error){
            Utils.printError(String.format("Element [%s] not found!", element));
        }
        //line berikut ini harusnya dihapus nanti, buat catatan dulu
        //driver().findElement(element(locator)).isDisplayed();
    }
}
//ini untuk membuat method yang sering dipakai > abstract dan bisa dipakai berulang-ulang
